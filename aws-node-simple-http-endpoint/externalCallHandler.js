module.exports.main = (event, context, callback) => {
    console.log('main function triggered') // appears in cloudwatch

    try {
        console.log('try block triggered') // appears in cloudwatch

        const superagent = require('superagent')
        console.log('superagent loaded', superagent) // appears in cloudwatch

        superagent.get('https://api.nasa.gov/planetary/apod')
            .query({
                api_key: 'DEMO_KEY',
                date: '2017-08-02'
            })
            .end((err, res) => {
                console.log('end function triggered') // MISSING in cloudwatch

                if (err) {
                    return console.log(err) // MISSING in cloudwatch
                }
                console.log(res.body.url) // MISSING in cloudwatch
                console.log(res.body.explanation) // MISSING in cloudwatch

                const response = {
                    statusCode: 200,
                    body: JSON.stringify({
                      message: res.body.explanation,
                    }),
                  };
                callback(null, response);
            })
        console.log('after superagent') // appears in cloudwatch

    } catch (e) {
        console.log('catch block ran') // does not appear in cloudwatch, as expected
        console.log(e) // does not appear in cloudwatch, as expected
        callback(null, failure({
            status: false
        }))
    }
}