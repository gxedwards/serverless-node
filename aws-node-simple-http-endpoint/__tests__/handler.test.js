const handler = require('../handler');
/**
 * JEST based unit test for getLocalGreeting. Example of farming off the other classes.
 */

test ('correct greeting is generated', () => {
    expect (handler.getLocalGreeting('en')).toBe('Hello!');
    expect (handler.getLocalGreeting('es')).toBe('¡Hola!');
});