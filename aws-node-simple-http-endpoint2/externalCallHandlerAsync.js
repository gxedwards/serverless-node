/**
 * AWS Lambda function call to external API. Uses Async keyword (v8.10 runtime)
 */
module.exports.main = async function(event, context) {
    console.log('main function triggered') 

    try {
        console.log('try block triggered') 

        callRemoteApi();

    } catch (e) {
        console.log('catch block ran');
        console.log(e);
        return failure({
            status: false
        });
    }
}

/**
 * Use superagent to do remote call. Equally could use request instead or the even lower http which is included in nodejs
 */
function callRemoteApi() {
    const superagent = require('superagent');
    console.log('superagent loaded', superagent);
    superagent.get('https://api.nasa.gov/planetary/apod')
        .query({
            api_key: 'DEMO_KEY',
            date: '2017-08-02'
        })
        .end((err, res) => {
            console.log('end function triggered');
            if (err) {
                return console.log(err);
            }
            console.log(res.body.url);
            console.log(res.body.explanation);
            const response = {
                statusCode: 200,
                body: JSON.stringify({
                    message: res.body.explanation,
                }),
            };
            return response;
        });
    console.log('after superagent');
}
