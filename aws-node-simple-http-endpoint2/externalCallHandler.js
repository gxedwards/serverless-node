module.exports.main = (event, context, callback) => {
    console.log('main function triggered. Hello World') // appears in cloudwatch

    try {
        var locations = require('mastercard-locations');
        var MasterCardAPI = locations.MasterCardAPI;

        var consumerKey = "wWm8WniKf4zcuY_gvpBLvV0WWJR2t5aRuQsYzH7C0ba0419a!20c943159b1a4944bbd6949205d1303e0000000000000000";   // You should copy this from "My Keys" on your project page e.g. UTfbhDCSeNYvJpLL5l028sWL9it739PYh6LU5lZja15xcRpY!fd209e6c579dc9d7be52da93d35ae6b6c167c174690b72fa
        var keyStorePath = "LocationsAPITest-sandbox.p12"; // e.g. /Users/yourname/project/sandbox.p12 | C:\Users\yourname\project\sandbox.p12
        var keyAlias = "keyalias";   // For production: change this to the key alias you chose when you created your production key
        var keyPassword = "keystorepassword";   // For production: change this to the key alias you chose when you created your production key

        // You only need to do initialize MasterCardAPI once
        // For production use pass sandbox: false
        var authentication = new MasterCardAPI.OAuth(consumerKey, keyStorePath, keyAlias, keyPassword);
        MasterCardAPI.init({
            sandbox: true,
            authentication: authentication
        });

        var requestData = {
            "PageLength": "5",
            "Latitude": event.queryStringParameters.latitude,
            "Longitude": event.queryStringParameters.longitude,
            "DistanceUnit": "MILE",
            "Radius": "25",
            "PageOffset": "0"
        };

        locations.ATMLocations.query(requestData, function (error, data) {
                if (error) {
                    console.error("An error occurred");
                    console.error(error);
                    callback(error, {

                        statusCode: 400,
                  
                        headers: { 'Content-Type': 'text/plain' },
                  
                        body: 'Something messed up.',
                  
                      });
                }
                else {
                    console.log(data.Atms.PageOffset);     //Output-->0
                    console.log(data.Atms.TotalCount);     //Output-->26

                    for (var i=0; i< data.Atms.TotalCount; i++){
                        let atm = data.Atms.Atm[i];
                        console.log(atm);                        
                    }

                    const response = {
                        statusCode: 200,
                        headers: { 'Content-Type': 'application/json' },
                        body: JSON.stringify({
                            atm: data.Atms,
                        }),
                    };
                    callback(null, response);
                    /* console.log(data.Atms.Atm[0].Location.Distance);     //Output-->0.9320591049747101
                    console.log(data.Atms.Atm[0].Location.DistanceUnit);     //Output-->MILE
                    console.log(data.Atms.Atm[0].Location.Address.Line1);     //Output-->4201 Leverton Cove Road
                    console.log(data.Atms.Atm[0].Location.Address.City);     //Output-->SPRINGFIELD
                    console.log(data.Atms.Atm[0].Location.Address.PostalCode);     //Output-->11101
                    console.log(data.Atms.Atm[0].Location.Address.CountrySubdivision.Name);     //Output-->UYQQQQ
                    console.log(data.Atms.Atm[0].Location.Address.CountrySubdivision.Code);     //Output-->QQ
                    console.log(data.Atms.Atm[0].Location.Address.Country.Name);     //Output-->UYQQQRR
                    console.log(data.Atms.Atm[0].Location.Address.Country.Code);     //Output-->UYQ
                    console.log(data.Atms.Atm[0].Location.Point.Latitude);     //Output-->38.76006576913497
                    console.log(data.Atms.Atm[0].Location.Point.Longitude);     //Output-->-90.74615107952418
                    console.log(data.Atms.Atm[0].Location.LocationType.Type);     //Output-->OTHER
                    console.log(data.Atms.Atm[0].HandicapAccessible);     //Output-->NO
                    console.log(data.Atms.Atm[0].Camera);     //Output-->NO
                    console.log(data.Atms.Atm[0].Availability);     //Output-->UNKNOWN
                    console.log(data.Atms.Atm[0].AccessFees);     //Output-->UNKNOWN
                    console.log(data.Atms.Atm[0].Owner);     //Output-->Sandbox ATM 1
                    console.log(data.Atms.Atm[0].SharedDeposit);     //Output-->NO
                    console.log(data.Atms.Atm[0].SurchargeFreeAlliance);     //Output-->NO
                    console.log(data.Atms.Atm[0].SurchargeFreeAllianceNetwork);     //Output-->DOES_NOT_PARTICIPATE_IN_SFA
                    console.log(data.Atms.Atm[0].Sponsor);     //Output-->Sandbox
                    console.log(data.Atms.Atm[0].SupportEMV);     //Output-->1
                    console.log(data.Atms.Atm[0].InternationalMaestroAccepted);     //Output-->1
 */
                }
            });



    } catch (e) {
        console.log('catch block ran') // does not appear in cloudwatch, as expected
        console.log(e) // does not appear in cloudwatch, as expected
        callback(null, failure({
            status: false
        }))
    }
}